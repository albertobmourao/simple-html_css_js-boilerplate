# Simple HTML_CSS_JS Boilerplate

This is a simple boilerplate with an HTML index file linked to a CSS and a JS file to help make faster the beginning of a project. 

## Files structure

- .
    - index.html
    - src
    - css
        - style.css
        - js
        - index.js
